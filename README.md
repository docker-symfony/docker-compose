# Docker Compose setup for Dockerized Symfony Application

This repo contains a Docker Compose example for running a [Dockerized Symfony Application](https://gitlab.com/docker-symfony/application).


## Setup

After you built the images in the [example application](https://gitlab.com/docker-symfony/application) execute the following:

```bash
cp docker-compose.override.yml.dist docker-compose.override.yml
docker-compose up -d
```
